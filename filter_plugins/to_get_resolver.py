from ansible import errors
from ipaddress import IPv4Address, IPv4Network
try:
    from __main__ import display
except ImportError:
    from ansible.utils.display import Display
    display = Display()


# print messages in debug mode
def debug_msg(msg):
    filter_prefix_debug = "[Filter to_get_resolver() DEBUG]"
    display.v("%s %s" % (filter_prefix_debug, msg))


# return resolvers list for datacenter
def get_resolvers_list(r_map, datacenter):
    resolvers_list = []
    for address in r_map[datacenter]:
        resolvers_list.append("nameserver %s" % address)
    return resolvers_list


# returns resolvers block for blockinfile ansible module
def to_get_resolver(host_ip, r_map, r_net_map):

    try:
        # loop over resolver_net_map{} from resolver ansible role variables
        for dc in r_net_map:

            # loop over list if more than zero ipv4 cidr in dc
            if len(r_net_map[dc]) >= 0:
                try:
                    # loop over resolver_net_map[dc] and check if host belongs to subnet
                    for network in r_net_map[dc]:
                        if IPv4Address(host_ip) in IPv4Network(network):
                            debug_msg("DC: %s, subnet: %s host: %s" %
                                      (dc.upper(), network, host_ip))
                            return get_resolvers_list(r_map, dc)

                except ValueError as e:
                    raise errors.AnsibleFilterError(
                        "DC: %s bad subnet %s" % (dc, e))

            else:
                debug_msg("DC: %s, no networks specified" % dc.upper())
    except ValueError as e:
        raise errors.AnsibleFilterError(
            "No resolvers found for datacenter: %s" % e)


def is_gcp_instance(ip, resolvers_map):
    try:
        for network in resolvers_map['gcp']:
            if IPv4Address(ip) in IPv4Network(network):
                debug_msg("Check if host %s belongs to GCP subnets" % ip)
                return True
            else:
                continue
        return False
    except Exception as e:
        raise errors.AnsibleFilterError("is_gcp_instance() exception: %s" % e)


class FilterModule(object):
    def filters(self):
        return {'to_get_resolver': to_get_resolver,
                'is_gcp_instance': is_gcp_instance}
